import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
  title = 'Angular 2';
  private timestamp = 1;
  // TODO: move that to config, putting it here for testing purposes for now.
  private apiKey = 'd38be787f1c4f29cdb8249df8a22466b';
  private hash = '57184d49f0683c463d228995708fa4e2';

  private baseUrl = 'https://gateway.marvel.com:443/v1/public/';

  total: number;
  count: number;
  pages: number;
  private limit = 20;

  constructor(private http: Http) {
  }

  getCharacters(page = 1) {
    if (page < 1) {
      return Observable.throw(new Error(`page number couldn't be less than 1`));
    }

    let offset = Math.abs((page - 1) * this.limit);
    return this.http
      .get(`${this.baseUrl}characters?apikey=${this.apiKey}&ts=${this.timestamp}&hash=${this.hash}&offset=${offset}`)
      .map((res: Response) => {
        let raw = res.json();
        let {code = 0, data: {results = [], total = this.total, count = this.count, limit = this.limit} } = raw;
        if (code < 200 || code > 300) {
          return Observable.throw(new Error(status));
        }

        this.total = total;
        this.limit = limit;
        this.count = count;
        this.pages = Math.ceil(this.total / this.limit);

        return results.map(r => new Character(r));
      });
  }

  getCharacter(id) {
    if (typeof id === 'undefined') {
      return Observable.throw(new Error(`ID couldn't be empty.`));
    }

    if (isNaN(id)) {
      return Observable.throw(new Error(`Invalid character ID.`));
    }

    return this.http
      .get(`${this.baseUrl}characters/${id}?apikey=${this.apiKey}&ts=${this.timestamp}&hash=${this.hash}`)
      .map((res: Response) => {
        let raw = res.json();
        let {code = 0, status, data: {results = []} } = raw;
        if (code < 200 || code > 300) {
          return Observable.throw(new Error(status));
        }

        if (results.length === 0) {
          return Observable.throw(new Error(`ID(${id}) couldn't be find.`));
        }

        return results.map(r => new Character(r));
      });
  }
}

enum Thumbnail {
  Small = 'portrait_small',
  Medium = 'portrait_medium',
  Large = 'portrait_xlarge',
  Fantastic = 'portrait_fantastic',
  Uncanny = 'portrait_uncanny',
  Incredible = 'portrait_incredible',
}

@Injectable()
export class Character {
  name: string;
  description: string;
  id: number;
  thumbnailUrl: string;
  detailsUrl: string;

  constructor(data: any) {
    this.name = data.name;
    this.description = data.description;
    this.id = data.id;
    this.thumbnailUrl = `${data.thumbnail.path}/${Thumbnail.Incredible}.${data.thumbnail.extension}`;

    try {
      this.detailsUrl = data.urls[0].url;
    } catch {
      // TODO: sigh, too lazy to add checks here.
      this.detailsUrl = '';
    }
  }
}
