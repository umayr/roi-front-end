describe('App', function () {

  beforeEach(function () {
    browser.get('/');
  });

  it('should have a title', function () {
    expect(browser.getTitle()).toEqual("ROI - Frontend Task");
  });

  // TODO: Add app component e2e tests
});
