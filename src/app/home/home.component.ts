import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ApiService, Character } from '../shared';

@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  characters: Array<Character> = [];
  error: Error = null;

  total: number;
  cursor: number;

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.route.queryParams.subscribe(({page}) => {
      if (typeof page !== 'undefined') {
        this.loadPage(null, page);
      }
    });

    this.route.params.subscribe(({ id }) => {
      if (typeof id === 'undefined') {
        id = null;
      }

      let page = this.route.snapshot.queryParams['page'];
      this.loadPage(id, page);
    });

    this.router.events.subscribe((e) => {
      if (!(e instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

  loadPage(id = null, page = 1) {
    if (id !== null) {
      this.api.getCharacter(id).subscribe(
        characters => this.characters = characters,
        error => this.error = error,
      );
      return;
    }

    if (typeof page !== 'undefined' && isNaN(page)) {
      this.error = new Error('Page number should be numeric');
      return;
    }

    this.api.getCharacters(page).subscribe(
      characters => {
        this.characters = characters;
        this.total = this.api.pages;
        this.cursor = page;
      },
      error => this.error = error,
    );
  }
  pageChange() {
    this.router.navigate(['/'], {queryParams: {page: this.cursor}});
  }
}
